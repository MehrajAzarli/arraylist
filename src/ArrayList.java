import java.util.Arrays;

public class ArrayList {
    private Object[] data;
    private int size;


    public ArrayList(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.data = new Object[initialCapacity];
        size = 0;
    }


    public ArrayList() {
        this(10);
    }


    public void add(Object item) {
        ensureCapacity();
        data[size++] = item;
    }

    public void remove(Object item) {
        int index = indexOf(item);
        if (index != -1) {
            removeAtIndex(index);
        }
    }


    public boolean contains(Object item) {
        return indexOf(item) != -1;
    }

    public void clear() {
        size = 0;

    }


    public boolean isEmpty() {
        return size == 0;
    }


    public void set(int index, Object item) {
        if (index >= 0 && index < size) {
            data[index] = item;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }


    public int size() {
        return size;
    }


    @Override
    public String toString() {
        if (size == 0) return "[]";

        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size - 1; i++) {
            sb.append(data[i]).append(", ");
        }
        return sb.append(data[size - 1]).append("]").toString();
    }


    private int indexOf(Object item) {
        for (int i = 0; i < size; i++) {
            if (data[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }


    private void removeAtIndex(int index) {
        for (int i = index; i < size - 1; i++) {
            data[i] = data[i + 1];
        }
        size--;
    }


    private void ensureCapacity() {
        if (size == data.length) {
            int newCapacity = size * 2;
            data = Arrays.copyOf(data, newCapacity);
        }
    }
}
